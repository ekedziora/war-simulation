package model

import scala.collection.SortedSet

object Suits extends Enumeration {
  type Suit = Value
  val Spades, Hearts, Diamonds, Clubs = Value

  def allCardsFrom(suit: Suit): Set[Card] = {
    CardValue.values.map(Card(suit, _))
  }
}

package model

import model.CardValue.CardValue

import scala.util.Random

class Deck(val cards: Seq[Card]) {
  def this() = this(
    (for {
      suit <- Suits.values
      value <- CardValue.values
    } yield Card(suit, value)).toList
  )

  val size: Int = cards.size

  val empty: Boolean = size == 0

  def shuffle = Deck(Random.shuffle(cards))

  def deal: GameState = {
    val dealt = cards.zipWithIndex.partition { case (_, idx) => idx % 2 == 0 }
    GameState(dealt._1.map(_._1), dealt._2.map(_._1))
  }

  def pickAndRemoveRandomCard: (Card, Deck) = {
    if (cards.isEmpty)
      throw new IllegalStateException("There are no more cards left in a deck")
    val randomIndex = Random.nextInt(cards.length)
    val randomCard = cards(randomIndex)
    (randomCard, removeCard(randomCard))
  }

  def pickAndRemoveCardWithValue(cardValue: CardValue): (Card, Deck) = {
    val card = cards.find(_.value == cardValue)
      .getOrElse(throw new IllegalStateException(s"There's no more cards in deck with value $cardValue"))
    (card, removeCard(card))
  }

  def removeCard(card: Card): Deck =
    Deck(cards.filterNot(_ == card))

  def take(n: Int): Deck = Deck(cards.take(n))
}

object Deck {
  def apply(cards: Seq[Card]) = new Deck(cards)
  def default = new Deck
  def empty = Deck(List.empty)

  val DEFAULT_DECK_SIZE = 52
}

package model

import model.GameState.COMMA_SEPARATOR
import model.GameState.EMPTY_STRING
import model.GameState.EMPTY_TABLE_CARDS_DESCRIPTION
import model.GameState.HIDDEN_CARD_DESCRIPTION
import model.GameState.PLAYER_ONE_WINS_DESCRIPTION
import model.GameState.PLAYER_TWO_WINS_DESCRIPTION
import model.GameState.WAR_DESCRIPTION

import scala.collection.immutable.Queue

case class GameState(playerOneCards: Queue[Card], playerTwoCards: Queue[Card],
                     playerOneTableCards: List[Card], playerTwoTableCards: List[Card], turnNumber: Int) {

  require(playerOneTableCards.size == playerTwoTableCards.size,
    s"Players table cards number is not equal (Player One: ${playerOneTableCards.size}, Player Two: ${playerTwoTableCards.size}")

  def this(playerOneCards: Queue[Card], playerTwoCards: Queue[Card], turnNumber: Int) =
    this(playerOneCards, playerTwoCards, List.empty, List.empty, turnNumber)

  val gameEnded: Boolean = playerOneTableCards.isEmpty && playerTwoTableCards.isEmpty && (playerOneCards.isEmpty || playerTwoCards.isEmpty)

  def simulateTurn: GameState =
    if (cardComparisonNeeded)
      evaluateOnCardComparison(giveTableCardsToPlayerOne, giveTableCardsToPlayerTwo, putNextCardsOnTableOrReturnTableCardsToPlayers)
    else
      putNextCardsOnTableOrReturnTableCardsToPlayers

  private def giveTableCardsToPlayerTwo =
    GameState(playerOneCards, playerTwoCards.enqueue(playerOneTableCards).enqueue(playerTwoTableCards), turnNumber)

  private def giveTableCardsToPlayerOne =
    GameState(playerOneCards.enqueue(playerOneTableCards).enqueue(playerTwoTableCards), playerTwoCards, turnNumber)

  private def putNextCardsOnTableOrReturnTableCardsToPlayers =
    (for {
      (firstPlayerCard, firstPlayerLeftCards) <- playerOneCards.dequeueOption
      (secondPlayerCard, secondPlayerLeftCards) <- playerTwoCards.dequeueOption
    } yield putNextCardsOnTable(firstPlayerCard, firstPlayerLeftCards, secondPlayerCard, secondPlayerLeftCards)
    ) getOrElse returnCardsToPlayers

  private def putNextCardsOnTable(firstPlayerCard: Card, firstPlayerLeftCards: Queue[Card], secondPlayerCard: Card, secondPlayerLeftCards: Queue[Card]) =
    GameState(firstPlayerLeftCards, secondPlayerLeftCards, firstPlayerCard :: playerOneTableCards,
      secondPlayerCard :: playerTwoTableCards, turnNumber + 1)

  private def returnCardsToPlayers =
    GameState(playerOneCards.enqueue(playerOneTableCards), playerTwoCards.enqueue(playerTwoTableCards), turnNumber)

  private def evaluateOnCardComparison[T](firstGreater: => T, secondGreater: => T, equal: => T): T =
    compareTableCards match {
      case 0 => equal
      case result if result > 0 => firstGreater
      case result if result < 0 => secondGreater
    }

  private val cardComparisonNeeded: Boolean = playerOneTableCards.size % 2 == 1

  private def compareTableCards: Int = playerOneTableCards.head.compareValues(playerTwoTableCards.head)

  def description: String =
    s"""############### TURN $turnNumber ###############
       |Player One cards ${playerOneCards.size}|${playerTwoCards.size} Player Two cards
       |Player One cards: $playerOneCards
       |Player Two cards: $playerTwoCards
       |Player One cards on table: ${tableCardsStringDescription(playerOneTableCards)}
       |Player Two cards on table: ${tableCardsStringDescription(playerTwoTableCards)}
       |$getTurnResultDescription
       |#########################################
     """.stripMargin

  private def getTurnResultDescription: String =
    if (cardComparisonNeeded)
      evaluateOnCardComparison(PLAYER_ONE_WINS_DESCRIPTION, PLAYER_TWO_WINS_DESCRIPTION, WAR_DESCRIPTION)
    else
      EMPTY_STRING

  private def tableCardsStringDescription(tableCards: List[Card]): String =
    if (tableCards.isEmpty)
      EMPTY_TABLE_CARDS_DESCRIPTION
    else
      tableCards.reverse.zipWithIndex.map { case (card, index) =>
        if (index % 2 == 0) card.toString else HIDDEN_CARD_DESCRIPTION
      } mkString COMMA_SEPARATOR
}

object GameState {
  val EMPTY_TABLE_CARDS_DESCRIPTION = "()"
  val HIDDEN_CARD_DESCRIPTION = "*"
  val COMMA_SEPARATOR = ", "
  val PLAYER_ONE_WINS_DESCRIPTION = "Player One wins!"
  val PLAYER_TWO_WINS_DESCRIPTION = "Player Two wins!"
  val WAR_DESCRIPTION = "War!"
  val FIRST_TURN_NUMBER = 1
  val EMPTY_STRING = ""

  def apply(playerOneCards: Queue[Card], playerTwoCards: Queue[Card], turnNumber: Int) =
    new GameState(playerOneCards, playerTwoCards, turnNumber)

  def apply(cards: (Seq[Card], Seq[Card])): GameState = new GameState(Queue(cards._1: _*), Queue(cards._2: _*), FIRST_TURN_NUMBER)

  def apply(playerOneCards: Queue[Card], playerTwoCards: Queue[Card], playerOneTableCards: List[Card], playerTwoTableCards: List[Card]) =
    new GameState(playerOneCards, playerTwoCards, playerOneTableCards, playerTwoTableCards, FIRST_TURN_NUMBER)

  def withNoTableCards(playerOneCards: Queue[Card], playerTwoCards: Queue[Card]): GameState =
    new GameState(playerOneCards, playerTwoCards, FIRST_TURN_NUMBER)
}
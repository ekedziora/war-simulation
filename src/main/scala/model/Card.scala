package model

import model.CardValue.CardValue
import model.Suits.Suit

case class Card(suit: Suit, value: CardValue) {
  def compareValues(other: Card): Int = value.compare(other.value)

  override def toString: String = s"($value of $suit)"
}

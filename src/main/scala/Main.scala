import model.Deck
import model.Deck.DEFAULT_DECK_SIZE
import model.GameState

import scala.annotation.tailrec

object Main {

  def main(args: Array[String]): Unit = {
    println("------------------ WAR - CARD GAME ------------------")
    println("Created by Emil Kędziora\n\n")
    val deck = resolveDeck(args)
    println(s"1. Shuffling deck (${deck.size} cards)")
    val shuffled = deck.shuffle
    println("2. Dealing")
    val dealt = shuffled.deal
    println("3. Game start")
    simulateGame(dealt)
  }

  @tailrec
  private def simulateGame(state: GameState): GameState = {
    if (state.gameEnded)
      state
    else {
      val newState = state.simulateTurn
      println(newState.description)
      simulateGame(newState)
    }
  }

  private def resolveDeck(args: Array[String]) = {
    if (args.length > 0) {
      toInt(args(0)).filter(i => i > 0 && i <= DEFAULT_DECK_SIZE)
        .map(Deck.default.take(_))
        .getOrElse(Deck.default)
    } else Deck.default
  }

  private def toInt(s: String): Option[Int] = {
    try {
      Some(s.toInt)
    } catch {
      case _: Exception => None
    }
  }

}

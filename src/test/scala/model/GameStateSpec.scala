package model

import model.CardValue.Ace
import model.CardValue.CardValue
import model.CardValue.Five
import model.CardValue.Jack
import model.CardValue.King
import model.CardValue.Nine
import model.CardValue.Queen
import model.CardValue.Seven
import org.scalatest.FlatSpec

import scala.collection.immutable.List
import scala.collection.immutable.Queue

class GameStateSpec extends FlatSpec {

  "Creation of game state" should "throw exception if table cards numbers are different for players" in new WithCardDeck {
    assertThrows[IllegalArgumentException] {
      GameState(Queue.empty, Queue.empty, List.empty, List(randomCard()))
    }
  }

  "Ended game turn simulation" should "return same game state" in new WithCardDeck  {
    val playerOneCards = Queue.empty
    val playerTwoCards = Queue(randomCard())
    val currentState = GameState.withNoTableCards(playerOneCards, playerTwoCards)
    val newState = currentState.simulateTurn
    assert(newState === currentState)
  }

  "Turn simulation for no cards on table" should "put next cards on table" in new WithCardDeck {
    val playerOneCards = Queue(randomCard())
    val playerTwoCards = Queue(randomCard())
    val newState = GameState.withNoTableCards(playerOneCards, playerTwoCards).simulateTurn
    assert(newState.playerOneCards.isEmpty)
    assert(newState.playerTwoCards.isEmpty)
    assert(newState.playerOneTableCards === List(playerOneCards: _*))
    assert(newState.playerTwoTableCards === List(playerTwoCards: _*))
  }

  "Turn simulation" should "correctly simulate player two win" in new WithCardDeck  {
    val playerOneCards = Queue.empty
    val playerTwoCards = Queue.empty
    val playerOneTableCards = List(randomCardWithValue(Nine))
    val playerTwoTableCards = List(randomCardWithValue(Jack))
    val newState = GameState(playerOneCards, playerTwoCards, playerOneTableCards, playerTwoTableCards).simulateTurn
    assert(newState.playerOneCards.isEmpty)
    assert(newState.playerTwoCards.toSet === (playerOneTableCards ++ playerTwoTableCards).toSet)
    assert(newState.playerOneTableCards.isEmpty)
    assert(newState.playerTwoTableCards.isEmpty)
  }

  "Turn simulation" should "correctly simulate player one win" in new WithCardDeck {
    val playerOneCards = Queue.empty
    val playerTwoCards = Queue.empty
    val playerOneTableCards = List(randomCardWithValue(Ace))
    val playerTwoTableCards = List(randomCardWithValue(CardValue.Three))
    val newState = GameState(playerOneCards, playerTwoCards, playerOneTableCards, playerTwoTableCards).simulateTurn
    assert(newState.playerTwoCards.isEmpty)
    assert(newState.playerOneCards.toSet === (playerOneTableCards ++ playerTwoTableCards).toSet)
    assert(newState.playerOneTableCards.isEmpty)
    assert(newState.playerTwoTableCards.isEmpty)
  }

  "Turn simulation for war" should "put first war card on table" in new WithCardDeck  {
    val playerOneCards = Queue(randomCard())
    val playerTwoCards = Queue(randomCard())
    val playerOneTableCards = List(randomCardWithValue(King))
    val playerTwoTableCards = List(randomCardWithValue(King))
    val newState = GameState(playerOneCards, playerTwoCards, playerOneTableCards, playerTwoTableCards).simulateTurn
    assert(newState.playerOneCards.isEmpty)
    assert(newState.playerTwoCards.isEmpty)
    assert(newState.playerOneTableCards === playerOneCards ++ playerOneTableCards)
    assert(newState.playerTwoTableCards === playerTwoCards ++ playerTwoTableCards)
  }

  "Turn simulation for war where player has no more cards" should "return cards to players hands" in new WithCardDeck  {
    val warCard = randomCard()
    val nextCard = randomCard()
    val playerOneCards = Queue.empty
    val playerTwoCards = Queue(nextCard)
    val tableCards = List(warCard)
    val newState = GameState(playerOneCards, playerTwoCards, tableCards, tableCards).simulateTurn
    assert(newState.playerOneCards === Queue(warCard))
    assert(newState.playerTwoCards === Queue(nextCard, warCard))
    assert(newState.playerOneTableCards.isEmpty)
    assert(newState.playerTwoTableCards.isEmpty)
  }

  "Turn simulation for first war card showed where player has no more cards" should "return cards to players hands" in new WithCardDeck  {
    val warCard = randomCard()
    val nextCard = randomCard()
    val playerOneCards = Queue.empty
    val playerTwoCards = Queue(nextCard)
    val tableCards = List(warCard, nextCard).reverse
    val newState = GameState(playerOneCards, playerTwoCards, tableCards, tableCards).simulateTurn
    assert(newState.playerOneCards.toSet === tableCards.toSet)
    assert(newState.playerTwoCards.head === nextCard)
    assert(tableCards.toSet.subsetOf(newState.playerTwoCards.toSet))
    assert(newState.playerOneTableCards.isEmpty)
    assert(newState.playerTwoTableCards.isEmpty)
  }

  "Turn simulation for first war card showed" should "put next cards on table" in new WithCardDeck  {
    val warCard = randomCard()
    val firstWarCard = randomCard()
    val newWarCard = randomCard()
    val playerOneCards = Queue(newWarCard)
    val playerTwoCards = Queue(newWarCard)
    val tableCards = List(warCard, firstWarCard).reverse
    val newState = GameState(playerOneCards, playerTwoCards, tableCards, tableCards).simulateTurn
    assert(newState.playerOneCards.isEmpty)
    assert(newState.playerTwoCards.isEmpty)
    assert(newState.playerOneTableCards === (newWarCard :: tableCards))
    assert(newState.playerTwoTableCards === (newWarCard :: tableCards))
  }

  "Turn simulation for second war card showed" should "simulate player one win" in new WithCardDeck  {
    val warCard = randomCard()
    val firstWarCard = randomCard()
    val playerOneCards = Queue.empty
    val playerTwoCards = Queue.empty
    val playerOneTableCards = List(warCard, firstWarCard, randomCardWithValue(King)).reverse
    val playerTwoTableCards = List(warCard, firstWarCard, randomCardWithValue(Five)).reverse
    val newState = GameState(playerOneCards, playerTwoCards, playerOneTableCards, playerTwoTableCards).simulateTurn
    assert(newState.playerOneCards.toSet === (playerOneTableCards ++ playerTwoTableCards ++ playerOneCards ++ playerTwoCards).toSet)
    assert(newState.playerTwoCards.isEmpty)
    assert(newState.playerOneTableCards.isEmpty)
    assert(newState.playerTwoTableCards.isEmpty)
  }

  "Turn simulation for second war card showed" should "simulate player two win" in new WithCardDeck  {
    val warCard = randomCard()
    val firstWarCard = randomCard()
    val playerOneCards = Queue.empty
    val playerTwoCards = Queue.empty
    val playerOneTableCards = List(warCard, firstWarCard, randomCardWithValue(Seven)).reverse
    val playerTwoTableCards = List(warCard, firstWarCard, randomCardWithValue(Queen)).reverse
    val newState = GameState(playerOneCards, playerTwoCards, playerOneTableCards, playerTwoTableCards).simulateTurn
    assert(newState.playerTwoCards.toSet === (playerOneTableCards ++ playerTwoTableCards ++ playerOneCards ++ playerTwoCards).toSet)
    assert(newState.playerOneCards.isEmpty)
    assert(newState.playerOneTableCards.isEmpty)
    assert(newState.playerTwoTableCards.isEmpty)
  }

  "Turn simulation for second war" should "take cards from players and put them on table" in new WithCardDeck  {
    val firstWarCard = randomCard()
    val secondWarCard = randomCard()
    val playerOneCards = Queue(randomCard())
    val playerTwoCards = Queue(randomCard())
    val playerOneTableCards = List(firstWarCard, randomCard(), secondWarCard).reverse
    val playerTwoTableCards = List(firstWarCard, randomCard(), secondWarCard).reverse
    val newState = GameState(playerOneCards, playerTwoCards, playerOneTableCards, playerTwoTableCards).simulateTurn
    assert(newState.playerOneCards.isEmpty)
    assert(newState.playerTwoCards.isEmpty)
    assert(newState.playerOneTableCards === (playerOneCards ++ playerOneTableCards))
    assert(newState.playerTwoTableCards === (playerTwoCards ++ playerTwoTableCards))
  }

  "Turn simulation for second war second card show" should "put next cards to winners hand" in new WithCardDeck  {
    val firstWarCard = randomCard()
    val secondWarCard = randomCard()
    val playerOneCards = Queue.empty
    val playerTwoCards = Queue.empty
    val playerOneTableCards = List(firstWarCard, randomCard(), secondWarCard, randomCard(), randomCardWithValue(Ace)).reverse
    val playerTwoTableCards = List(firstWarCard, randomCard(), secondWarCard, randomCard(), randomCardWithValue(Seven)).reverse
    val newState = GameState(playerOneCards, playerTwoCards, playerOneTableCards, playerTwoTableCards).simulateTurn
    assert(newState.playerOneCards.toSet === (playerOneTableCards ++ playerTwoTableCards ++ playerOneCards ++ playerTwoCards).toSet)
    assert(newState.playerTwoCards.isEmpty)
    assert(newState.playerOneTableCards.isEmpty)
    assert(newState.playerTwoTableCards.isEmpty)
  }

  "Turn simulation for second war second card show where next war begins" should "put next cards on table" in new WithCardDeck  {
    val firstWarCard = randomCard()
    val secondWarCard = randomCard()
    val thirdWarCard = randomCard()
    val playerOneCards = Queue(randomCard())
    val playerTwoCards = Queue(randomCard())
    val playerOneTableCards = List(firstWarCard, randomCard(), secondWarCard, randomCard(), thirdWarCard).reverse
    val playerTwoTableCards = List(firstWarCard, randomCard(), secondWarCard, randomCard(), thirdWarCard).reverse
    val newState = GameState(playerOneCards, playerTwoCards, playerOneTableCards, playerTwoTableCards).simulateTurn
    assert(newState.playerOneCards.isEmpty)
    assert(newState.playerTwoCards.isEmpty)
    assert(newState.playerOneTableCards === playerOneCards ++ playerOneTableCards)
    assert(newState.playerTwoTableCards === playerTwoCards ++ playerTwoTableCards)
  }

  "Game ended value" should "be true if one of players has no cards and there's no cards on table" in new WithCardDeck {
    val playerOneCards = Queue.empty
    val playerTwoCards = Queue(randomCard())
    val tableCards = List.empty
    val state = GameState(playerOneCards, playerTwoCards, tableCards, tableCards)
    assert(state.gameEnded)
  }

  "Game ended value" should "be false if both players has cards" in new WithCardDeck {
    private val playerOneCards = Queue(randomCard())
    private val playerTwoCards = Queue(randomCard())
    private val tableCards = List.empty
    private val state = GameState(playerOneCards, playerTwoCards, tableCards, tableCards)
    assert(!state.gameEnded)
  }

  "Game ended value" should "be false if there are cards on table" in new WithCardDeck {
    private val playerOneCards = Queue()
    private val playerTwoCards = Queue()
    private val tableCards = List(randomCard())
    private val state = GameState(playerOneCards, playerTwoCards, tableCards, tableCards)
    assert(!state.gameEnded)
  }

  private trait WithCardDeck {
    var deck: Deck = Deck.default

    def randomCard(): Card = {
      val (card, newDeck) = deck.pickAndRemoveRandomCard
      deck = newDeck
      card
    }

    def randomCardWithValue(cardValue: CardValue): Card = {
      val (card, newDeck) = deck.pickAndRemoveCardWithValue(cardValue)
      deck = newDeck
      card
    }
  }

}

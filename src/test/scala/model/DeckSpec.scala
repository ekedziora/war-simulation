package model

import org.scalatest.FlatSpec

import scala.collection.immutable.Queue

class DeckSpec extends FlatSpec {

  "Newly created deck" should "contains all cards" in {
    val deck = Deck.default
    val allCards = for {
      suit <- Suits.values
      value <- CardValue.values
    } yield Card(suit, value)
    val deckCards = deck.cards.toSet
    assert(deck.size === allCards.size)
    assert(deckCards === allCards)
  }

  "Shuffle for default deck" should "return deck with different ordered cards" in {
    val deck = Deck.default
    val shuffled = deck.shuffle
    assert(deck.size === shuffled.size)
    assert(deck.cards !== shuffled.cards)
  }

  "Shuffle for empty deck" should "return other empty deck" in {
    val deck = Deck.empty
    val shuffled = deck.shuffle
    assert(deck.size === shuffled.size)
    assert(shuffled.empty)
  }

  "Deal action for default deck" should "evenly split cards between players" in {
    val game = Deck.default.deal
    assert(game.playerOneCards.size === game.playerTwoCards.size)
  }

  "Deal action for empty deck" should "serve no cards to players" in {
    val game = Deck.empty.deal
    assert(game.playerOneCards.isEmpty)
    assert(game.playerTwoCards.isEmpty)
  }

  "Deal action for one card deck" should "serve card to first player" in {
    val onlyCard = Card(Suits.Clubs, CardValue.Ace)
    val game = Deck(Seq(onlyCard)).deal
    assert(game.playerTwoCards.size === 0)
    assert(game.playerOneCards === Queue(onlyCard))
  }

  "Deal action for custom deck" should "give every element with even index to first player and every element with odd index to second player" in {
    val cards = Suits.allCardsFrom(Suits.Clubs).toList
    val game = Deck(cards).deal
    assert(game.playerOneCards === evens(cards))
    assert(game.playerTwoCards === odds(cards))
  }

  private def elementsWithIndexDividedByTwoEqualToN[T](l: List[T], n: Int) = l.view.zipWithIndex.filter(_._2 % 2 == n).map(_._1)
  private def evens[T](l: List[T]) = elementsWithIndexDividedByTwoEqualToN(l, 0)
  private def odds[T](l: List[T]) = elementsWithIndexDividedByTwoEqualToN(l, 1)

}
